class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :email
      t.string :password_digest
      t.string :name
      t.text :address
      t.integer :contact
      t.integer :admin, default: 0, null: false

      t.timestamps
    end
  end
end

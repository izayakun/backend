class CreateOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :orders do |t|
      t.integer :user_id
      t.integer :pet_id
      t.text :ship_address
      t.integer :quantity
      t.float :amount
      t.string :status

      t.timestamps
    end
  end
end

class Order < ApplicationRecord
    belongs_to :user
    belongs_to :pet

    default_scope -> { order(created_at: :desc) }

    validates :quantity, presence: true
    validates :ship_address, presence: true
end

class Pet < ApplicationRecord
    has_many :orders

    validates :name, presence: true, length: { maximum: 100 }
    validates :price, presence: true
end

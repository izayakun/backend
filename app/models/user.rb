class User < ApplicationRecord
  include ActiveModel::Serializers::JSON
  has_secure_password
  has_many :orders, dependent: :destroy

  validates :email,
            format: { with: URI::MailTo::EMAIL_REGEXP },
            presence: true,
            uniqueness: { case_sensitive: false }

  validates :password, presence: true, length: { minimum: 5 }
end

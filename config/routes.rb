Rails.application.routes.draw do
  post 'refresh', controller: :refresh, action: :create
  post 'signin', controller: :signin, action: :create
  post 'signup', controller: :signup, action: :create
  delete 'signout', controller: :signin, action: :destroy

  namespace :api do
    namespace :v1 do
      resources :pets
      resources :users

      resources :orders do
        get 'cart/:id', to: 'orders#cart', on: :collection
        get 'purchase/:id', to: 'orders#purchase', on: :collection
        post 'checkout/:id/', to: 'orders#create', on: :collection
        post '/:id', to: 'orders#status', on: :collection
      end
    end
  end
end
